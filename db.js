var MongoClient = require('mongodb').MongoClient
	, assert = require('assert')
	;

// Connection URL
var url = 'mongodb://localhost:27017/multiplayer';
MongoClient.connect(url, function(err, db) {
	assert.equal(err, null);
	console.log("Connected Correctly to server");

	//Create a collection
	var collection = db.collection("dishes");//need new collection name

	//Insert an item into a collection
	collection.insertOne({name: "Pizza", description: "Test"}
		, function(err, result) {
			assert.equal(err, null);
			console.log("After Insert: ");
			console.log(result.ops);

			//Find all items in a collection
			collection.find({}).toArray(function(err, docs) {
				assert.equal(err, null);
				console.log("Found:")
				console.log(docs);

				//Delete a collection
				db.dropCollection("dishes", function(err, result) {
					assert.equal(err, null);
					db.close();
				})
			});
		});
});
