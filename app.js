// TODO: MOBILE VERSION
var express = require('express')
	, cors = require('cors')
	, app = express()
	, main = require('./routes/main.js')
	, templates = require('./routes/templates.js')
	, postRoute = require('./routes/post.js')
	, timeLog = require('./_middleware/timeLog.js').log
	, render = require('./_middleware/render.js')
	, server = app.listen(3000, function() {  console.log("Listening on port 3000");  })
	, io = require('socket.io').listen(server)
	, clientList = {}
	, msgList = []
	;

app.use(cors());
app.engine('html',require('ejs').renderFile);
app.set('views', __dirname + '/views');
app.set('view engine ', 'html');
app.use('/', (req, res, next) => {req.skeleton = {}; console.log(''); next()}, timeLog);
app.use(express.static(__dirname + '/public'));

//GET ROUTES
app.route('/menu').get(templates.menuPage, render.sendDataToPage);
app.route('/').get(main.index, timeLog, render.renderDataToFile.bind(this, 'index.html'));

//POST ROUTES
app.route('/*').post((req, res) => { 
	var path = req.url;
	console.log("Post: ", req.body);
	postRoute(req.body, path);
});

//Note: socket refers to the individual socket a user is connected on.
//		io refers to the server and will send to everyone currently connected.
io.on('connection', socket => {
	
	console.log("user connected");
	//port these functions over to a socket route later

	//should be a broadcast, msgList should be moved to only send to the unique user
	socket.emit('connection');
	
	socket.on('addChatMessage', (msg, username) => {
		var fullmsg = username + ": " + msg
			;
		console.log("adding chat message");
		msgList.push(fullmsg);
		if(msgList.length > 200)
			msgList.shift();
		postRoute({message: msg, author: username, display: `${username}: ${msg}`}, 'addChat');
    	io.emit('addChatMessage', fullmsg);
  	});

 	socket.on('new user', (username) => {
 		console.log("new user!", username);
 		socket.username = username;
    	clientList[username] = username;
    	io.sockets.emit('user list', clientList);
    	socket.broadcast.emit('chat message', username + ' has connected');
 	});

 	socket.on('disconnect', () => {
 		disconnect1(this);
 	});
 	socket.on('logout', () => {
 		disconnect1(this);
 	})
});

function disconnect1(socket) {
	delete clientList[socket.username];
	io.sockets.emit('user list', clientList);
	if(socket.username)
		socket.broadcast.emit('chat message', socket.username + ' has disconnected.');
	console.log("user disconnected");
}