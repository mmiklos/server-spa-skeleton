var render = module.exports = {}

render.renderDataToFile = function(fileName, req, res) {
	res.render(fileName, req.skeleton);
}

render.sendDataToPage = function(req, res) {
	res.contentType('text/html');
	res.send(res.body);
}