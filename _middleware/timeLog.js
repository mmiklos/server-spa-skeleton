timeLog = module.exports = {};
timeLog.log = function(req, res, next) {
	if(req.skeleton && req.skeleton.timeInit) {
		req.skeleton.timeFin = Date.now();
		req.skeleton.timeTotal = req.skeleton.timeFin - req.skeleton.timeInit;
		console.log("Server finished in: " + req.skeleton.timeTotal + " ms")
	} else 
		req.skeleton.timeInit = Date.now();
	
	next();
}