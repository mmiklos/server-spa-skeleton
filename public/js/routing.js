'use strict';
var socket = socket || io()
	, hashList = {}
	;

//this is good and all, but I'll need to do some research into cacheing to see if I should go any further with this, or if its just wasting server cpu
window.onload = function initializeRouter() {
	console.log("window ready");
	document.getElementById('windowLoad').style.display = 'none';
	
	window.onhashchange = Router.bind(this, function() {
		//incase I need this for later
		//window.location.hash = "";
	});

	if(!window.location.hash)//we have redirects elsewhere which can go off before window.onload is fired
		window.location.hash = "home";
	else
		Router();
}

var Router = (function() {
	//****************************************************************************************Maps
	var hashMap = {
			'#home': {
				name: '#home'
				, fn: homePage
				, includeAbstracts: false
			}
			, '#mainmenu': {
				name: '#mainmenu'
				, fn: mainmenu
				, includeAbstracts: true
			}
		}
		//abstracts represent partials which are on consistent on every page.
		, abstracts = [
			chatBox
		]
		;


	//****************************************************************************************Full Pages
	
	function homePage() {
		console.log("home page");
	}

	function mainmenu() {
		//this should really be serving files and not strings. Then we can check the cache? Ultimately its the game that will take time to load, not the HTML
		//this url is to serve all pages which branch from the menu page
		get(GLOBALSELFURL + '/menu', function(data) {
			appendToBody(data);
		});
	}

	//****************************************************************************************Abstracts

	function chatBox() {
		var parent = document.getElementById('widgetContainer')
			;

		if(parent && typeof(Chat) === 'function')
			Chat(parent);
		else 
			console.log("error appending chat");
	}

	//****************************************************************************************Middleware

	function render(url) {
		var renderObj = getHashPage(url)
			, hashPage = renderObj.page
			, i = 0
			;
		//execute the function and return its value
		if(renderObj.error)
			renderObj.error();
		else {
			if(hashPage.includeAbstracts)
				for(i = 0; i < abstracts.length; i++) {
					if(typeof(abstracts[i]) === 'function')
						abstracts[i]();
					else
						console.log("Error: Abstract", i, "is not a function!");
				}
			hashList[hashPage.name] = true;
			hashPage.fn();
			console.log("HL", hashList)

		}
	}
		
	function getHashPage(url) {
		var hash = url.split('/')[0];

		console.log("hash url", url)
		if(hashMap[hash])
			return { page: hashMap[hash] };
		else 
			return { error: errorPage };
	}

	function errorPage() {
		console.log("error");
		return false;
	}

	return function(cb) {
		if(window.location.hash && !hashList[window.location.hash]) {
			render(decodeURI(window.location.hash));
			if(typeof(cb) === 'function')
				cb();
		}
	}

})();


function appendToBody(o) {
	var html = o//should be an object for later
		, div = document.createElement('div')
		;

	div.innerHTML = html;
	document.getElementsByTagName('body')[0].appendChild(div);
}

function post(url, data, cb) {
	httpRequest('POST', url, data, function(res) {
		if(typeof(cb) === 'function')
			cb(res);
	});
}

function get(url, cb) {
	httpRequest('GET', url, null, function(data) {
		if(typeof(data) === 'string') {
			try {
				data = JSON.parse(data);
			}
			catch(e) {
				console.log('data is not json');
			}
		}
		if(typeof(cb) === 'function')
			cb(data);
	});
}

function httpRequest(method, url, data, cb) {
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			if(typeof(cb) === 'function')
				cb(xhttp.responseText);
		} else {
			console.log("error: ", xhttp.status, url);
		}
	}

	xhttp.open(method, url, true);
	if(method === 'POST')
		xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send();
}