'use strict';

var socket = socket || io()
	;

var Chat = (function() {

	function constructPreviousChat() {
		get(GLOBALURL + '/chatLogs', function(data) {
			console.log("Your Data: ", data, data.length)
			for(var i = data.length-1; i >= 0; i--) {
				newMessage(data[i].display);
			}
		});
	}
	
	function newMessage(msg) {
		var li = document.createElement('li')
			;

		li.innerHTML = msg;
		document.getElementById('messages').appendChild(li);
	}

	 function buildChat() {
	 	var html = '';

	 	html += '<div id="messaging" class="">';

	    html += '<ul id="messages"></ul>';
	    
	    html += '<div class="bottomDisplay">Current Users Online: ';
		html += '    <span id="usersOnline">0</span> ';
		html += '    <span id="userList"></span>';
	  	html += '</div>';
	    
	    html += '<form id="chat">';
	    html += '  <input id="m" class="chatBox" autocomplete="off" /><button>Send</button>';
	    html += '  <input type="hidden" id="u" autocomplete="off" />';
	    html += '</form>';

	  	html += '</div>';

	  	return html;
	}


	function DOMuserList(userList) {
		var element = document.getElementById('userList')
			, count = document.getElementById('usersOnline')
			, userArray = []
			, key = null
			;

		console.log("Users: ", userList)
		count.innerHTML = Object.keys(userList).length;

		for (key in userList) {
	        if (userList.hasOwnProperty(key)) {
	        	userArray.push(userList[key])
	        }
	    }
		element.innerHTML = userArray.join(", ");
	}


	return function(DOMcontainer) {
		DOMcontainer.innerHTML = buildChat();

		var chatForm = document.getElementById('chat');
		chatForm.onsubmit = function (event) {
			var m = ''
				, form = event.target
				;
			console.log("Form", form)
			if(this.elements && this.elements.m) {
				m = this.elements.m;
				socket.emit('addChatMessage',  m.value, GLOBALUSERNAME);
				// post('/addChat', form, function() {

				// })
				m.value = '';
			}

			return false;//prevent form submit
		}

		//*****************************************************************************CHAT SOCKET LISTENERS
		socket.on('addChatMessage', function(msg){
			newMessage(msg);
	 	});

	 	socket.on('connection', function(){
	 		constructPreviousChat();
	 	});

	 	socket.on('user list', function(userList) {
			DOMuserList(userList);
		});

	}


}());


