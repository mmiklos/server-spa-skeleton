'use strict';
//this page will be broken up later
var socket = socket || io()
	, GLOBALUSERNAME = 'Guest'
	, GLOBALSELFURL = 'http://localhost:3000'
	, GLOBALURL = 'http://localhost:4000'
	;

(function ready() {

var userID = document.getElementById('userID')
	, logoutButton = document.getElementById('logout')
	, userName = localStorage.getItem('sessionID')
	;

	if(userName)
		setSessionID(userName);
	
	//----------------------------------------------------------------------------Navigation: Home -> Main Menu
	userID.onsubmit = function() {
		var sessionID = '';

		if(this && this.elements && this.elements.inputUserID)
			sessionID = this.elements.inputUserID.value;

		setSessionID(sessionID);
		return false;
	};

	logoutButton.onclick = logout;

})(window);


function setPageTitle(sessionID) {
	var t = document.getElementById('pageTitle')
		, title = "Welcome " + sessionID;
		;
	t.innerHTML = title;
}

function logout() {
	localStorage.removeItem('sessionID');
	socket.emit('logout');
	window.location.hash = 'home';
}

function setSessionID(id) {
	var sessionID = id || 'Guest'
		;

	if(localStorage) {
		if(sessionID !== 'Guest')
			localStorage.setItem('sessionID', sessionID);
		socket.emit('new user', sessionID);
		GLOBALUSERNAME = sessionID;
		setPageTitle(sessionID);
	} else {
		//browser does not support localstorage, do nothing for now
	}

	console.log("new user", sessionID);
	window.location.hash = '#mainmenu';
}