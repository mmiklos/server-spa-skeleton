# Single Page Application (SPA) Using Express and basic javascript and sockets#

While this project wasn't built to look visually appealing, its purpose is to dynamically render content to the screen for the user as fast as possible by offsetting the initial load content to be the minimal amount possible, and then periodically loading in more using promises or based off user navigation. I should be continually maintaining this code to use it as a skeleton in the future for other private projects... It still needs a bunch of work though...

###Main Project TODOs:
* Learn the details of the details of page caching -.>
* Construct a real login system through redis