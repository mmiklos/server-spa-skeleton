var express = require('express');
var http = require('http');
//This will need to handle all posts
//Initially it will recieve req.body (post data) and post url
//we want to:
//	1. validate the data if necessary
//	2. format the data
//	3. send the data
var post = module.exports = postToAPI;
function postToAPI(data, path) {
	var apiRoute = 'localhost:4000';//temp
	var map = {
		'addChat':'chatLogs'
	}
	console.log("Path: ", path, map[path]);
	console.log("Data: ", data);

	if(data) {
		if(typeof(data) === 'object')
			data = JSON.stringify(data);

		var options = {
		    host: 'localhost',
		    port: 4000,
		    path: `/${map[path]}`,
		    method: 'POST',
			headers: {
				  'Content-Type': 'application/json',//x-www-form-urlencoded',
				  'Content-Length': Buffer.byteLength(data)
			}
		};

	 	var httpreq = http.request(options, function (response) {
		    response.setEncoding('utf8');
			response.on('data', function (chunk) {
			  console.log("body: " + chunk);
			});
			response.on('end', function() {
			  //res.send('ok');
			})
		});
		httpreq.write(data);
		httpreq.end();
	}
}

