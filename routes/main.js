
//route: '/'
var main = module.exports = {};
main.index = function(req, res, next) {
	//note: req.skeleton object assignment shares memory, 
	//we do not need to reassign data to req.skeleton at the end
	var UA = req.headers['user-agent']
		, data = req.skeleton
		, mobile = (UA.indexOf('Android') !== -1 || UA.indexOf('iPhone') !== -1) 
			? true
			: false
		;
	
	data.json = {'String': 'This is Main'};

	if(mobile)
		main.mobileIndex(req, res, next)
	else {
		console.log("Route: Main.index");
		next();
	}
}

main.mobileIndex = function(req, res, next) {
	var data = req.skeleton;
	console.log("Route: Main.mobileIndex");
	data.mobile = true;
	next();
}